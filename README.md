## Premier TD

[schema projet](https://gitlab.com/sducher/DUCHER_M4207/raw/master/Files/Schema_Projet.pdf)

#### Ex0 

```c
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  delay(200);
  Serial.print("Hello world !");
  digitalWrite(13,LOW);
  delay(200);
}
``` 

#### Ex1
```c
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(13,OUTPUT);
}

int x=0;
void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  delay(200);
  Serial.print("x = ");
  Serial.print(x);
  Serial.print("\n\n");
  x+=1;
  digitalWrite(13,LOW);
  delay(200);
}
```

### Ex2

```c 

Voilà ce qui apparait dans le sérial


UTC timer 12-37-13
latitude =  8960.0000, longitude =     0.0000
satellites number = 0
LGPS loop
$GPGGA,123715.086,8960.0000,N,00000.0000,E,0,0,,137.0,M,13.0,M,,*4E

```c
### Ex3 Wifi

```c

Voilà ce que me retourne le code après m'être connecté à mon téléphone

Connecting to AP
SSID: DarkMi5SPlus
IP Address: 192.168.43.137
subnet mask: 255.255.255.0
gateway IP: 192.168.43.1
signal strength (RSSI):-67 dBm
Start Server
Server Started



